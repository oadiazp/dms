from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.conf.urls.static import static
from django.contrib import admin
from dms import settings

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^accounts/', include('apps.accounts.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^ucistore_finder/', include('apps.ucistore_finder.urls')),
    url(r'^', include('apps.core.urls')),
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

