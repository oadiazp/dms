#!/bin/bash
set -e
APP_NAME=dms

NUM_WORKERS=3

# user/group to run as
USER=ubuntu
GROUP=ubuntu

LOGFILE=/home/$USER/apps/$APP_NAME/src/log/gunicorn.log
LOGDIR=$(dirname $LOGFILE)

cd /home/$USER/apps/$APP_NAME/src/

test -d $LOGDIR || mkdir -p $LOGDIR

exec gunicorn_django -c /home/$USER/apps/$APP_NAME/src/gunicorn.conf -w $NUM_WORKERS \
--user=$USER --group=$GROUP --log-level=debug \
--log-file=$LOGFILE 2>>$LOGFILE
