# -*- coding: utf-8 -*-

from __future__ import with_statement
from fabric.api import env, require, run, sudo, cd, prefix
from fabric.contrib.files import exists

#--------------------------------------
#environments
#--------------------------------------


def local():
    "Servidor local de prueba"
    env.name = 'local'
    env.user = 'zcool'
    env.project_name = 'dms'
    env.project_root = '/home/%(user)s/apps/%(project_name)s/src' % env
    env.hosts = ['10.52.13.240']
    env.branch = 'master'
    env.repo = 'http://oadiaz@git.comunidades.prod.uci.cu'
    env.venv = '/home/%(user)s/apps/%(project_name)s/venv' % env


def deploy():
    "Servidor local de prueba"
    env.name = 'deploy'
    env.user = 'ubuntu'
    env.project_name = 'dms'
    env.project_root = '/home/%(user)s/apps/%(project_name)s/src' % env
    env.hosts = ['dms.zczoft.com']
    env.branch = 'master'
    env.repo = 'https://oadiazp@bitbucket.org/oadiazp/dms.git'
    env.venv = '/home/%(user)s/apps/%(project_name)s/venv' % env
    env.use_ssh_config = True


def install_ubuntu_dependencies():
    sudo('apt-get -y install python-pip python-psycopg2 nginx gcc python2.7-dev')
    sudo('apt-get -y install python-ldap python-django-auth-ldap python-virtualenv')


def common():
    with cd(env.project_root):
        #sincronizar los modelos
        run('./manage.py syncdb')

        #correr las migraciones
        run('./manage.py migrate')

        #recojer los estáticos
        run('./manage.py collectstatic')

        #compilar los mensajes
        apps = ['accounts', 'core', 'downloader']

        for app in apps:
            with cd("%s/apps/%s" % (env.project_root, app)):
                run('./../../manage.py compilemessages')

        #actualizar los settings de nginx
        sudo("cp -Rf conf/%s/nginx.conf /etc/nginx/sites-available/%s.conf" % (env.name, env.project_name))

        #actualizar los settings de gunicorn
        sudo("cp -Rf conf/%s/gunicorn.conf %s" % (env.name, env.project_root))
        sudo("cp -Rf conf/%s/gunicorn.sh %s" % (env.name, env.project_root))

        #actualizar los settings de la app
        sudo("cp -Rf conf/%s/local_settings.py %s" % (env.name, env.project_root))

        #reiniciar el servicio
        sudo("service nginx restart")
        sudo('supervisorctl restart %s' % env.project_name)

def clone_repository():
    run('git clone %s %s' % (env.repo, env.project_root))

    with cd(env.project_root):
        run('git checkout %s' % env.branch)


def install_pip_dependencies():
    with cd(env.project_root):
        sudo('pip install -r requirements.pip')


def update():
    require('name')
    require('project_root')

    with cd(env.project_root):
        #darle un pull al servidor
        run('git pull')


def install():
    install_ubuntu_dependencies()
    clone_repository()
    install_pip_dependencies()
    common()