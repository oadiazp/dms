$(function() {
  var avgrundContent, chatWindow, updateScrollFeeds;
  avgrundContent = "";
  avgrundContent += "<aside class=\"\">";
  avgrundContent += "  <div class=\"modal-header\">";
  avgrundContent += "    <button type=\"button\" class=\"close avgrund-close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>";
  avgrundContent += "    <h3>Modal header</h3>";
  avgrundContent += "  </div>";
  avgrundContent += "  <div class=\"modal-body\">";
  avgrundContent += "    <p>One fine body…</p>";
  avgrundContent += "  </div>";
  avgrundContent += "  <div class=\"modal-footer\">";
  avgrundContent += "    <a href=\"#\" class=\"btn avgrund-close\">Close</a>";
  avgrundContent += "    <a href=\"#\" class=\"btn btn-primary\">Save changes</a>";
  avgrundContent += "  </div>";
  avgrundContent += "</aside>";
  $("#show-avgrund").avgrund({
    height: 170,
    width: 560,
    holderClass: "custom",
    showClose: false,
    enableStackAnimation: true,
    onBlurContainer: ".wraper",
    template: avgrundContent
  });
  chatWindow = $(".chat-messages-list .content");
  if (ie === 8) {
    chatWindow.slimScroll({
      height: '400px'
    });
  } else {
    chatWindow.slimScroll({
      start: "bottom",
      railVisible: true,
      alwaysVisible: true,
      height: '400px'
    });
  }
  $("[data-toggle='popover']").popover({
    container: "#main",
    trigger: "hover"
  });
  $("[data-toggle='tooltip']").tooltip();
  $("#gritters #add-regular").click(function() {
    $.gritter.add({
      title: "This is a regular notice!",
      text: "This will fade out after a certain amount of time. Vivamus eget tincidunt velit. Cum sociis natoque penatibus et <a href=\"#\" style=\"color:#ccc\">magnis dis parturient</a> montes, nascetur ridiculus mus.",
      image: url_avatar,
      sticky: false,
      time: ""
    });
    return false;
  });
  $("#gritters #add-sticky").click(function() {
    var unique_id;
    unique_id = $.gritter.add({
      title: "This is a sticky notice!",
      text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus eget tincidunt velit. Cum sociis natoque penatibus et <a href=\"#\">magnis dis parturient</a> montes, nascetur ridiculus mus.",
      image: url_avatar,
      sticky: true,
      time: "",
      class_name: "gritter-light"
    });
    return false;
  });
  $("#gritters #add-without-image").click(function() {
    $.gritter.add({
      title: "This is a notice without an image!",
      text: "This will fade out after a certain amount of time. Vivamus eget tincidunt velit. Cum sociis natoque penatibus et <a href=\"#\">magnis dis parturient</a> montes, nascetur ridiculus mus.",
      class_name: "gritter-light"
    });
    return false;
  });
  $("#gritters #add-max").click(function() {
    $.gritter.add({
      title: "This is a notice with a max of 3 on screen at one time!",
      text: "This will fade out after a certain amount of time. Vivamus eget tincidunt velit. Cum sociis natoque penatibus et <a href=\"#\">magnis dis parturient</a> montes, nascetur ridiculus mus.",
      image: "http://a0.twimg.com/profile_images/59268975/jquery_avatar_bigger.png",
      sticky: false,
      class_name: "gritter-light",
      before_open: function() {
        if ($(".gritter-item-wrapper").length === 3) {
          return false;
        }
      }
    });
    return false;
  });
  $("#gritters #remove-all").click(function() {
    $.gritter.removeAll();
    return false;
  });
  $("#gritters #add-gritter-light").click(function() {
    $.gritter.add({
      title: "This is a light notification",
      text: "Just add a \"gritter-light\" class_name to your $.gritter.add or globally to $.gritter.options.class_name",
      class_name: "gritter-light"
    });
    return false;
  });
  /* Activate the scrollbar for the feed lists
  */

  updateScrollFeeds = function() {
    return $("#feeds .content").slimScroll({
      height: '445px'
    });
  };
  $("#feeds a[data-toggle=\"tab\"]").on("shown", function(e) {
    updateScrollFeeds();
  });
  updateScrollFeeds();
  /* Date Range Picker
  */

  $('#reservation').daterangepicker();
  $("#reportrange").daterangepicker({
    ranges: {
      Today: ["today", "today"],
      Yesterday: ["yesterday", "yesterday"],
      "Last 7 Days": [
        Date.today().add({
          days: -6
        }), "today"
      ],
      "Last 30 Days": [
        Date.today().add({
          days: -29
        }), "today"
      ],
      "This Month": [Date.today().moveToFirstDayOfMonth(), Date.today().moveToLastDayOfMonth()],
      "Last Month": [
        Date.today().moveToFirstDayOfMonth().add({
          months: -1
        }), Date.today().moveToFirstDayOfMonth().add({
          days: -1
        })
      ]
    },
    opens: "left",
    format: "MM/dd/yyyy",
    separator: " to ",
    startDate: Date.today().add({
      days: -29
    }),
    endDate: Date.today(),
    minDate: "01/01/2012",
    maxDate: "12/31/2013",
    locale: {
      applyLabel: "Submit",
      fromLabel: "From",
      toLabel: "To",
      customRangeLabel: "Custom Range",
      daysOfWeek: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"],
      monthNames: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
      firstDay: 1
    },
    showWeekNumbers: true,
    buttonClasses: ["btn-danger"],
    dateLimit: false
  }, function(start, end) {
    $("#reportrange span").html(start.toString("MMMM d, yyyy") + " - " + end.toString("MMMM d, yyyy"));
  });
  $("#reportrange span").html(Date.today().add({
    days: -29
  }).toString("MMMM d, yyyy") + " - " + Date.today().toString("MMMM d, yyyy"));
  $("#daterange").daterangepicker({
    minDate: "01/01/2010",
    maxDate: "12/31/2015",
    showDropdowns: true
  });
  $("#limited").daterangepicker({
    dateLimit: {
      days: 3
    }
  });
  /* Pulsate
  */

  $("#pulse").pulsate({
    color: "#2c467e"
  });
  $(".pulse1").pulsate({
    glow: false,
    color: "#2c467e"
  });
  $(".pulse2").pulsate({
    color: "#f89406"
  });
  $(".pulse3").pulsate({
    reach: 100,
    color: "#222222"
  });
  $(".pulse4").pulsate({
    speed: 2500,
    color: "#51a351"
  });
  $(".pulse5").pulsate({
    pause: 1000,
    color: "#bd362f"
  });
  $(".pulse6").pulsate({
    onHover: true,
    color: "#e9eaed"
  });
});
