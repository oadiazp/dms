/* Calls the handler function if the user has clicked outside the object
*/

(function($) {
  return $.fn.extend({
    clickOutside: function(handler, exceptions) {
      var $this;
      $this = void 0;
      $this = this;
//      $("body").bind("click", function(event) {
//        if (exceptions && $.inArray(event.target, exceptions) > -1) {
//
//        } else {
//            content = null
//
//            try {
//                content = $.contains($this[0], event.target)
//                if (!content) {
//                    return handler(event, $this);
//                }
//            }
//
//            catch(TypeError) {
//
//            }
//        }
//      });
      return this;
    }
  });
})(jQuery);

/* Need this to make IE happy
   see http://soledadpenades.com/2007/05/17/arrayindexof-in-internet-explorer/
*/
if (!Array.indexOf) {
  Array.prototype.indexOf = function(obj) {
    var i;
    i = 0;
    while (i < this.length) {
      if (this[i] === obj) {
        return i;
      }
      i++;
    }
    return -1;
  };
}
