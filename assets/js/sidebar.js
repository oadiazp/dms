$(function() {
    /*=============================================================
    * Codes for the Social Sidebar */

    /*=== User settigs Menu ===*/
  var expadSidebar, fixeSidebarScroll, reduceSidebar, resizeHandler, sidebarScroll, sidebarScrollOptions, socialBarContainer, userSettingsContainer, wrapperCantainer;
  socialBarContainer = $(".social-sidebar");
  wrapperCantainer = $(".wraper");
  userSettingsContainer = $(".social-sidebar .user-settings");
    // Hide user-settings menu when click on its content
  userSettingsContainer.find(".user-settings-content").on("click", function(e) {
    e.stopPropagation();
    userSettingsContainer.toggle();
  });
    // Hide user-settings menu when click on its link in the footer area
  userSettingsContainer.find(".user-settings-footer a").on("click", function(e) {
    e.stopPropagation();
    userSettingsContainer.toggle();
  });
    // Handle when click outside the user settings menu
  userSettingsContainer.clickOutside(function(event, obj) {
      /* if it's clicked the user settings trigger (small upper left icon)
         the user settings menu will be hidden or shown **/
    if (event.target.className.indexOf("trigger-user-settings") >= 0) {
      obj.toggle();
      /* If we dom't click on the trigger or the user settings menu, this will
        be hide */
    } else {
      obj.hide();
    }
  });
  /*=== Hide/Show Sidebar ===*/
  // This will expad the size of the sidebar
  expadSidebar = function() {
    $(".wraper").addClass("sidebar-full").removeClass("sidebar-icon");
    socialBarContainer.addClass("sidebar-full");
    socialBarContainer.find(".accordion-group.active .accordion-body").collapse("show");
  };
  // This will reduce the size of the sidebar
  reduceSidebar = function() {
    $(".wraper").removeClass("sidebar-full");
    socialBarContainer.removeClass("sidebar-full");
    socialBarContainer.find(".accordion-body.in").collapse("hide");
    userSettingsContainer.hide();
  };
  $(".switch-sidebar-icon").click(function() {
    reduceSidebar();
  });

  $(".switch-sidebar-full").click(function() {
    expadSidebar();
  });
  socialBarContainer.on("mouseleave", function(event) {
    if ($(this).hasClass("auto-hide") && ($(window).width() > 480)) {
      reduceSidebar();
    }
  });
  socialBarContainer.on("mouseenter", function(event) {
    if ($(this).hasClass("auto-hide") && ($(window).width() > 480)) {
      expadSidebar();
    }
  });
  // Action when a menu of the sidebar will be shown
  $(".accordion-body").on("show", function() {
    $(this).parent().find(".accordion-toggle").addClass("opened");
      // If the sidebar if reduced we expand it
    if (!$(".wraper").hasClass("sidebar-full")) {
      $(".social-sidebar").addClass("sidebar-full");
      $(".wraper").addClass("sidebar-full");
      $(".wraper").addClass("sidebar-icon");
    }
  });
    // Action when a menu of the sidebar will be hidden
  $(".accordion-body").on("hide", function() {
    $(this).parent().find(".accordion-toggle").removeClass("opened");
      // If the sidebar if reduced we make sure it will be reduced
    if ($(".wraper").hasClass("sidebar-icon")) {
      $(".social-sidebar").removeClass("sidebar-full");
      $(".wraper").removeClass("sidebar-full");
    }
  });
    // Action when we click outside the sidebar
  socialBarContainer.clickOutside(function(event, obj) {
      /* If the sidebar is reduced mode, and we expand it, whe
         we click outside the sidebar it will be reduced*/
    if ($(".wraper").hasClass("sidebar-icon")) {
      $(".social-sidebar").removeClass("sidebar-full");
      $(".wraper").removeClass("sidebar-full");
      obj.find(".accordion-body.in").collapse("hide");
        // This collapse out each collapsible element in the menu
      obj.find(".accordion .accordion-group").each(function() {
        $(this).find(".accordion-toggle").removeClass("opened");
        $(this).find(".accordion-body").removeClass("in");
      });
    }
  });
    /*=== Sidebar Scroll ===*/
    /* Init Options for the scroll of the sidebar*/
  sidebarScroll = $(".social-sidebar-content .scrollable");
  sidebarScrollOptions = {
    height: "auto",
    size: "8px",
    railVisible: true,
    railColor: "#000"
  };
    /* This function handle the sidebar scrroll whe the window is resized*/
  resizeHandler = function(slimScrollelement, slimScrollparent) {
    var element, height, parent, windowHeight;
    element = slimScrollelement;
    parent = $(slimScrollparent);
    windowHeight = $(window).height();
    element.css("height", windowHeight + "px");
    parent.find(".slimScrollDiv").css("height", windowHeight + "px");
    height = Math.max((element.outerHeight() / element.scrollHeight) * element.outerHeight(), 30);
    parent.find(".slimScrollBar").css({
      height: height + "px"
    });
  };
  fixeSidebarScroll = function() {
    if ($(window).width() <= 979) {
      $(".social-sidebar .slimScrollDiv").attr('style', '');
      if (socialBarContainer.hasClass("in")) {
        sidebarScroll.css("height", $(window).height() - $(".social-navbar").height());
        $(".social-sidebar-content").css("height", $(document).height());
      }
    }
  };
  sidebarScroll.slimscroll(sidebarScrollOptions);
  $(window).resize(function() {
    if ($(window).width() > 979) {
      resizeHandler(sidebarScroll, ".social-sidebar-content");
    }
    fixeSidebarScroll();
  });
  socialBarContainer.on("show", function() {
    sidebarScroll.css("height", $(window).height() - $(".social-navbar").height());
    fixeSidebarScroll();
  });
  // This update the scroll when an element of the side is shown
  $(".social-sidebar .accordion-body").on("shown", function() {
    if ($(window).width() > 979) {
      sidebarScroll.slimscroll(sidebarScrollOptions);
    } else {
      fixeSidebarScroll();
    }
  });
    // This update the scroll when an element of the side is hidenof the side
  $(".social-sidebar .accordion-body").on("hidden", function() {
    if ($(window).width() > 979) {
      sidebarScroll.slimscroll(sidebarScrollOptions);
    } else {
      fixeSidebarScroll();
    }
  });
    /* We make sure that the user settings menu will be hiden whe we scroll
       the sidebar */
  sidebarScroll.bind("slimscrolling", function(e, pos) {
    if (userSettingsContainer.css("display") !== "none") {
      userSettingsContainer.hide();
    }
  });
  if (typeof chatboxManager !== 'undefined') {
    chatboxManager.init({
      sender: {
        username: "Me",
        lastname: "Me"
      }
    });
    $(".chat-users li > a").click(function(event, ui) {
      var id;
      id = $(this).attr("data-userid");
      chatboxManager.addBox(id, {
        title: "chatbox" + id,
        firstname: $(this).attr("data-firstname"),
        lastname: $(this).attr("data-lastname"),
        status: $(this).attr("data-status")
      });
      event.preventDefault();
    });
  }
});
