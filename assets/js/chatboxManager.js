var chatboxManager = function() {

    // list of all opened boxes
    var boxList = new Array();
    // list of boxes shown on the page
    var showList = new Array();
    // list of first names, for in-page demo
    var nameList = new Array();
    // sender info
    var sender = new Array();

    var config = {
        width : 230, //px
        gap : 5,
        maxBoxes : 3,
        sender: {username: "undefinied", lastname: "undefinied"},
        messageSent : function(id, to, msg, status) {
            chatboxManager.addBox(id);
            $("#" + id).chatbox("option", "boxManager").addMsg(config.sender.lastname, msg);

            // This repeat the sent message is the user is online
            // Just for demo
            if(status == "online"){
                setTimeout((function() {
                  $("#" + id).chatbox("option", "boxManager").addMsg(to, msg);
                }), 500);
            }
        }
    };

    var init = function(options) {
        $.extend(config, options)
    };


    var delBox = function(id) {
    // TODO
    };

    var getNextOffset = function() {
        return (config.width + config.gap) * showList.length;
    };

    var boxClosedCallback = function(id) {
        // close button in the titlebar is clicked
        var idx = showList.indexOf(id);
        if(idx != -1) {
            showList.splice(idx, 1);
            diff = config.width + config.gap;
            for(var i = idx; i < showList.length; i++) {
            offset = $("#" + showList[i]).chatbox("option", "offset");
            $("#" + showList[i]).chatbox("option", "offset", offset - diff);
            }
        }
        else {
            alert("should not happen: " + id);
        }
    };

    // caller should guarantee the uniqueness of id
    var addBox = function(id, user, name) {
        var idx1 = showList.indexOf(id);
        var idx2 = boxList.indexOf(id);
        if(idx1 != -1) {
            // found one in show box, do nothing
            $("#"+id).parent().show();
            $("#"+id).parent().find(".ui-chatbox-input-box").focus();
        }
        else if(idx2 != -1) {
            // exists, but hidden
            // show it and put it back to showList
            $("#"+id).chatbox("option", "offset", getNextOffset());
            var manager = $("#"+id).chatbox("option", "boxManager");
            manager.toggleBox();
            showList.push(id);
        }
        else{
            var el = document.createElement('div');
            el.setAttribute('id', id);
            $(el).chatbox(
                {
                   id : id,
                   user : user,
                   sender: sender,
                   title : '<i class="icon-circle user-status '+user.status+'"></i>' + user.firstname + " " + user.lastname,
                   hidden : false,
                   width : config.width,
                   offset : getNextOffset() + config.gap,
                   messageSent : messageSentCallback,
                   boxClosed : boxClosedCallback
                }
            );
            $(el).parent().find(".ui-chatbox-input-box").focus();
            $(el).parent().addClass(user.status);
            boxList.push(id);
            showList.push(id);
            nameList.push(user.firstname);
        }
    };

    var messageSentCallback = function(id, user, msg) {
        var idx = boxList.indexOf(id);
        config.messageSent(id, nameList[idx], msg, user.status);
    };

    // not used in demo
  //   var dispatch = function(id, user, msg) {
        // $("#" + id).chatbox("option", "boxManager").addMsg(user.firstname, msg);
  //   }

    return {
        init : init,
        addBox : addBox,
        delBox : delBox,
        //dispatch : dispatch
    };
}(jQuery);