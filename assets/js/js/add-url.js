$('.url-form .btn-primary').bind('click', function(e){
    var id = $('#request').val()

    $.ajax({
        url: '/request/' + id + '/urls/add/',
        data: {
            url: $('#id_url').val(),
            password: $('#id_password').val(),
            username: $('#id_username').val()
        },
        dataType: 'json',
        type: 'post',
        success: function(resp){
            response = Base64.decode(resp.response)

            try{
                obj = JSON.parse(response)
                $('.url-form').modal('hide')
                urls_grid.fnReloadAjax()
            }
            catch (e){
                $('.url-form').html(response)
            }
        }
    })
})