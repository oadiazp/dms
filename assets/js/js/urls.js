
function onUrlsGridReload(){
    $('table button.btn-danger').bind('click', function(e){
        var elem = $(e.target)

        if (! elem.hasClass('btn'))
            elem = elem.parent('button')

        var idfam = elem.attr('data-id')

        $('.message-form').attr('data-id', idfam)

        $('.message-form').modal('show')
    })

    $('.message-form .btn-danger').bind('click', function(e){
        var url_id = $('.message-form').attr('data-id')
        var request_id = $('#request').val()

        $.ajax({
            url: '/request/' + request_id + '/urls/' + url_id + '/remove/',
            type: 'post',
            success: function() {
                urls_grid.fnReloadAjax()
                $('.message-form').modal('hide')
            }
        })
    })
}

if (!$.fn.dataTable.fnIsDataTable($('table'))) {
    var id = $('#request').val()

    var urls_grid = $('table').dataTable({
        sPaginationType: "bootstrap",
        bServerSide: true,
        bProcessing: true,
        sAjaxSource: '/request/'+id + '/list_urls',
        fnDrawCallback: function() {
            onUrlsGridReload()
        }
    })
}