$(function() {
    function onRequestsGridReload() {
        $('table button.btn-danger').bind('click', function(e){
            var elem = $(e.target)

            if (! elem.hasClass('btn'))
                elem = elem.parent('button')

            var idfam = elem.attr('data-id')

            $('.message-form').attr('data-id', idfam)

            $('.message-form').modal('show')
        })

        $('.message-form .btn-danger').bind('click', function(e){
            var id =  $('.message-form').attr('data-id')

            $.ajax({
                url: '/requests/' + id + '/remove/',
                type: 'post',
                success: function() {
                    requests_grid.fnReloadAjax()
                    $('.message-form').modal('hide')
                }
            })
        })

        $('table button.btn-url').bind('click', function(e){
            var elem = $(e.target)

            if (! elem.hasClass('btn'))
                elem = elem.parent('button')

            var id = elem.attr('data-id')

            window.location.href = '/request/' + id + '/urls/'
        })
    }

    if (!$.fn.dataTable.fnIsDataTable($('table'))) {
        var requests_grid = $('table').dataTable({
            sPaginationType: "bootstrap",
            bServerSide: true,
            bProcessing: true,
            sAjaxSource: '/list_requests/',
            fnDrawCallback: function() {
                onRequestsGridReload()
            }
        })
    }
})