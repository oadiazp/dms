$(function() {
    function onRequestsGridReload() {
        $('table button').bind('click', function(e){
            if (confirm('Are you sure want approve this request?')) {
                var elem = $(e.target)

                if (! elem.hasClass('btn'))
                    elem = elem.parent('button')

                var idfam = elem.attr('data-id')

                $.ajax({
                    url: '/requests/' + idfam + '/approve/',
                    type: 'post',
                    success: function() {
                        requests_grid.fnReloadAjax()
                    }
                })
            }
        })
    }

    if (!$.fn.dataTable.fnIsDataTable($('table'))) {
        var requests_grid = $('table').dataTable({
            sPaginationType: "bootstrap",
            bServerSide: true,
            bProcessing: true,
            sAjaxSource: '/unapproved_requests/',
            fnDrawCallback: function() {
                onRequestsGridReload()
            }
        })
    }
})