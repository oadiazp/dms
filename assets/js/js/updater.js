$(function(){
    $('#id_name').typeahead({
        source: function(query, p){
            resp = []

            if (query.length > 3) {
                $.ajax({
                    url: '/ucistore_finder/find/',
                    type: 'get',
                    async: false,
                    dataType: 'json',
                    data: {
                        query: query
                    },
                    success: function(r){
                        resp = r
                    }
                })
            }

            return resp
        }
    })
})