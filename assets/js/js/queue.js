$(function() {
    function onRequestsGridReload() {
        $('table button.btn-up').bind('click', function(e){
            if (confirm('Are you sure want increment this request priority?')) {
                var elem = $(e.target)

                if (! elem.hasClass('btn'))
                    elem = elem.parent('button')

                var idfam = elem.attr('data-id')

                $.ajax({
                    url: '/requests/' + idfam + '/priority/',
                    type: 'post',
                    data: {
                        operation: '+'
                    },
                    success: function() {
                        requests_grid.fnReloadAjax()
                    }
                })
            }
        })

        $('table button.btn-down').bind('click', function(e){
            if (confirm('Are you sure want decrement this request priority?')) {
                var elem = $(e.target)

                if (! elem.hasClass('btn'))
                    elem = elem.parent('button')

                var idfam = elem.attr('data-id')

                $.ajax({
                    url: '/requests/' + idfam + '/priority/',
                    type: 'post',
                    data: {
                        operation: '-'
                    },
                    success: function() {
                        requests_grid.fnReloadAjax()
                    }
                })
            }
        })
    }

    if (!$.fn.dataTable.fnIsDataTable($('table'))) {
        var requests_grid = $('table').dataTable({
            sPaginationType: "bootstrap",
            bServerSide: true,
            bProcessing: true,
            sAjaxSource: '/requests_queue/',
            fnDrawCallback: function() {
                onRequestsGridReload()
            }
        })
    }
})