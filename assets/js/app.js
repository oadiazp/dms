
$(function() {
  var autohideTrigger, dividersTrigger, initialClasses, navbarColor, sidebar, socialNavbar, wraper;
  $("[href|='#']").click(function(e) {
    return e.preventDefault();
  });
  /*  Sidebar Options
  */

  sidebar = $(".social-sidebar");
  wraper = $(".wraper");
  dividersTrigger = $("#panel #sidebar-dividers");
  autohideTrigger = $("#panel #sidebar-autohide");
  if ($.cookie("sidebar_dividers") === "true") {
    sidebar.addClass("dividers");
    dividersTrigger.attr('checked', true);
  } else {
    sidebar.removeClass("dividers");
    dividersTrigger.attr('checked', false);
  }
  /*  Navbar Options
  */

  initialClasses = "navbar navbar-fixed-top social-navbar";
  socialNavbar = $(".social-navbar");
  navbarColor = $.cookie('navbar_color');
  if (typeof navbarColor !== 'undefined') {
    socialNavbar.attr('class', initialClasses);
    return socialNavbar.addClass(navbarColor);
  }
});
