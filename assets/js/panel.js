$(function() {
  /* Panel Trigger
  */
  $("#panel").clickOutside(function(event, obj) {
    var jqtarget;
    jqtarget = $(event.target);
    if (($(jqtarget).attr("id") === "template-settings") || ($(jqtarget).parent().attr("id") === "template-settings")) {
      obj.toggleClass("open", 500, "easeOutSine");
    } else {
      obj.removeClass("open", 500, "easeOutSine");
    }
  });
  /*  Sidebar Options
  */

  $("#panel #sidebar-dividers").click(function() {
    var $sidebar;
    $sidebar = $(".social-sidebar");
    if ($(this).prop("checked")) {
      $sidebar.addClass("dividers");
      $.cookie('sidebar_dividers', 'true');
    } else {
      $sidebar.removeClass("dividers");
      $.cookie('sidebar_dividers', 'false');
    }
  });
  /*  Navbar Options
  */

  $("select[name=\"colorpicker\"]").simplecolorpicker();
  $("select[name=\"colorpicker\"]").on("change", function() {
    var element, initialClasses, socialNavbar;
    initialClasses = "navbar navbar-fixed-top social-navbar";
    socialNavbar = $(".social-navbar");
    element = $("option:selected", this);
    socialNavbar.attr('class', initialClasses);
    socialNavbar.addClass(element.attr("data-class"));
    $.cookie('navbar_color', element.attr("data-class"));
  });
});
