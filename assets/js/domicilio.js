$('.btn-guardar-domicilio').bind('click', function(e) {
    var id = $('#id_empleado').val()

    $.ajax({
        url: '/domicilio/' + id + '/',
        type: 'post',
        dataType: 'json',
        data: {
            direccion_exacta: $('#id_direccion_exacta').val(),
            canton: $('#id_canton').val(),
            provincia: $('#id_provincia').val(),
            distrito: $('#id_distrito').val(),
            tiempo_residencia: $('#id_tiempo_residencia').val()
        },
        success: function(resp) {
            response = Base64.decode(resp.response)

            try{
                obj = JSON.parse(response)
                $('.modal-general').modal('show')
            }
            catch (e){
                $('#lC').html(response)
            }
        }
    })
})