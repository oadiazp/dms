"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

from django.test import TestCase
from django.test.client import Client


class SimpleHTTPTest(TestCase):
    def setUp(self):
        TestCase.setUp(self)
        self.client = Client()
    
    def login(self, user=None, password=None):
        u = user or self.user
        p = password or self.password
        self.assertNotEqual(u, None, 'El usuario no es valido')
        self.assertNotEqual(p, None, 'El password no es valido')
        self.assertTrue(self.client.login(username=u, password=p), 'Imposible autenticar al usuario')
    
    def __test_http_get__(self, url):
        if url:
            response = self.client.get(url)
            self.assertEqual(response.status_code, 200) 
    
    def __test_http_post__(self, url, data):
        response = self.client.post(url, data, follow=True)
        self.assertEqual(response.status_code, 200)
        return response
    
    def __test_success_response__(self, response):
        print response.content
        self.assertContains(response=response, text='PGh0bWw+Cgk8Ym9keT5bc3VjY2Vzc108L2JvZHk+CjwvaHRtbD4K', count=1)
        
    def __test_http_post_and_success_response__(self, url, data):
        response = self.__test_http_post__(url, data)
        self.__test_success_response__(response)
 
