# -*- coding: utf-8 -*-
from django import forms
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.core.validators import validate_email
from django.utils.translation import ugettext_lazy as _

class RecipientsField(forms.Field):
    
    email_recipients = []
    
    def to_python(self, value):
        if not value:
            return []      
        return [a for a in value.split('|')]
    
    def validate(self, value):
        forms.Field.validate(self, value)
        #Validando que los recipients sean usuarios o direcciones de correo validas
        for dest in value :
            users = User.objects.filter(username=dest)
            if not users:
                try:
                    validate_email(dest)
                    self.email_recipients.append(dest)
                except ValidationError:
                    raise ValidationError(_(u"%s no es un usuario o correo válido." % dest))
                
            else:
                self.email_recipients.append(users[0].email)

    def to_emails(self):
        return self.email_recipients