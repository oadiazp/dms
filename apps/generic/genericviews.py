from django.contrib import messages
from django.forms.forms import NON_FIELD_ERRORS
from django.forms.util import ErrorList
from django.http import HttpResponse, HttpResponseRedirect
from django.utils.translation import ugettext_lazy as _
from django.views.generic.base import TemplateView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic.list import ListView
from apps.generic.mixin import MultiFormatResponseMixin

import json
import logging

logger = logging.getLogger(__name__)

json_response = 'json'
default_response = 'default'


class JSONListView(MultiFormatResponseMixin, ListView):
    context_object_name = "object_list"
    serializer_options = {'json': {}}
    content_type = 'application/json'


class JSONDetailView(MultiFormatResponseMixin, DetailView):
    serializer_options = {'json': {}}
    content_type = 'application/json'


class JSONTemplateView(MultiFormatResponseMixin, TemplateView):
    serializer_options = {'json': {}}
    content_type = 'application/json'


class GenericUpdateView(UpdateView):
    success_url = '/success/'
    encode = 'base64'
    response_type = default_response
    form_invalid_context_name = 'form'

    def form_valid(self, form):
        try:
            self.object = form.save(commit=True)
            if hasattr(form,'messages'):
                for msg in form.messages:
                    messages.add_message(self.request, msg.msg_type, msg.msg_body)
        except Exception, e:
            form._errors.setdefault(NON_FIELD_ERRORS, ErrorList()).append(_(u'Se produjo un error.'))
            response = GenericUpdateView.form_invalid(self, form)
            logger.error(e, extra={'request': self.request,})
            return response
        else:
            if self.response_type == json_response :
                response = '{"success": true}'
                json_output = json.dumps({ 'encode': self.encode, 'response': response.encode(self.encode) })
                return HttpResponse(json_output)

            return HttpResponseRedirect(self.get_success_url())

    def render_to_response(self, context, **response_kwargs):
        response = UpdateView.render_to_response(self, context, **response_kwargs)
        rendered_content = response.render()
        json_output = json.dumps({ 'encode': self.encode, 'response': rendered_content.content.encode(self.encode) })
        return HttpResponse(json_output)


    def form_invalid(self, form):
        d = {
            self.form_invalid_context_name: form
        }

        return self.render_to_response(self.get_context_data(**d))



class GenericCreateView(CreateView):
    success_url = '/success/'
    encode = 'base64'
    response_type = default_response
    form_invalid_context_name = 'form'

    def form_valid(self, form):
        try:
            self.object = form.save(commit=True)
            if hasattr(form, 'messages'):
                for msg in form.messages:
                    messages.add_message(self.request, msg.msg_type,
                                         msg.msg_body)
        except Exception, e:
            response = CreateView.form_invalid(self, form)
            logger.error(e, extra={'request': self.request, })
            return response
        else:
            if self.response_type == json_response:
                response = '{"success": true}'
                json_output = json.dumps({'encode': self.encode,
                                          'response': response.encode(self.encode)})
                return HttpResponse(json_output)

            return HttpResponseRedirect(self.get_success_url())

    def render_to_response(self, context, **response_kwargs):
        response = CreateView.render_to_response(self, context,
                                                 **response_kwargs)
        rendered_content = response.render()
        json_output = json.dumps({'encode': self.encode,
                                  'response': rendered_content.content.encode(self.encode)})
        return HttpResponse(json_output)

    def form_invalid(self, form):
        d = {
            self.form_invalid_context_name: form
        }

        return self.render_to_response(self.get_context_data(**d))


class AuthenticatedCreateView(GenericCreateView):

    def get_form_kwargs(self):
        kwargs = super(AuthenticatedCreateView, self).get_form_kwargs()
        kwargs['current_user'] = self.request.user
        return kwargs


class AuthenticatedUpdateView(GenericUpdateView):

    def get_form_kwargs(self):
        kwargs = super(AuthenticatedUpdateView, self).get_form_kwargs()
        kwargs['current_user'] = self.request.user
        return kwargs
