# -*- coding: utf-8 -*-
import logging

logger = logging.getLogger(__name__)

###############################################################################
# Clase generica para todos los tipos dinamicos.
###############################################################################


class IDynamicObject:

    """
        Clase que implementan todos los transportes genericos
        que resume el metodo para obtener un objeto de forma
        dinamica dado su nombre de modulo, y su nombre de clase
    """

    @staticmethod
    def get_dynamic_object(object_module):
        """
            Retorna el objeto dinamico asociado a los datos o None en
            caso de error
            params:
                'object_module': El referencia completa separada por puntos
                del modulo python donde se encuentra la clase.

                'object_class:   El nombre completo de la clase que se desea
                cargar dinamicamente
        """
        # Obtenemos el listado de partes separadas por punto
        modules_parts = object_module.split('.')
        # El modulo es la union de todas las partes menos la ultima.
        module = ".".join(modules_parts[:-1])
        result = None

        try:
            result = __import__(module)
        except ImportError, e:
            logger.error(e)

        if result:
            for component in modules_parts[1:]:
                if hasattr(result, component):
                    result = getattr(result, component)
                else:
                    result = None
                    break

        return result()
