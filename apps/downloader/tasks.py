# -*- coding: utf-8 -*-

from celery.decorators import task
from apps.core.models import Profile


@task
def download_a_request(request):
    admin = Profile.objects.get(user__username='admin')
    request.process(admin)
