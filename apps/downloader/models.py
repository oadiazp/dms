from django.db import models


class RequestError(models.Model):
    request_status = models.OneToOneField('core.RequestStatus')
    message = models.CharField(max_length=200, default='')
    url = models.URLField()
