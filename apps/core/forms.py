from django import forms
from django.forms import ModelForm
from apps.core.models import Request, Tag, Url, Credential
from django.forms.util import ErrorList


class RequestForm(ModelForm):
    tags = forms.CharField(widget=forms.Textarea())

    class Meta:
        model = Request
        fields = ('name', 'description', 'partial')

    def __init__(self, profile, data=None, files=None, auto_id='id_%s',
                 prefix=None,initial=None, error_class=ErrorList,
                 label_suffix=':', empty_permitted=False, instance=None):
        super(RequestForm, self).__init__(data, files, auto_id, prefix, initial,
                                          error_class, label_suffix,
                                          empty_permitted, instance)
        self.profile = profile

    def save(self, commit=True):
        request = Request()
        request.description = self.cleaned_data['description']
        request.name = self.cleaned_data['name']
        request.user = self.profile
        request.partial = self.cleaned_data['partial']
        request.save()

        tags = self.cleaned_data['tags'].split(',')

        for tag_name in tags:
            tag = None

            try:
                tag = Tag.objects.get(denomination=tag_name)

            except Tag.DoesNotExist:
                tag = Tag()
                tag.denomination = tag_name
                tag.save()

            finally:
                request.tags.add(tag)

        request.add_status('New', self.profile)

        return request


class UrlForm(ModelForm):
    username = forms.CharField(required=False)
    password = forms.CharField(required=False,
                               widget=forms.PasswordInput)

    def __init__(self, request, data=None, files=None, auto_id='id_%s', prefix=None,
                 initial=None, error_class=ErrorList, label_suffix=':',
                 empty_permitted=False, instance=None):
        super(UrlForm, self).__init__(data, files, auto_id, prefix, initial,
                                      error_class, label_suffix,
                                      empty_permitted, instance)
        self.request = request


    class Meta:
        model = Url

    def save(self, commit=True):
        obj = super(UrlForm, self).save(commit)

        if 'username' in self.cleaned_data:
            c = Credential()
            c.username = self.cleaned_data['username']
            c.password = self.cleaned_data['password']
            c.url = obj
            c.save()

            self.request.urls.add(obj)

        return obj
