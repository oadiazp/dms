from django.contrib.sites.models import Site
from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
import os
import requests
from virtualenv import mkdir
from apps.downloader.models import RequestError

from dms.settings import UPLOAD_FILES_ROOT, DOWNLOAD_PATH, MEDIA_ROOT, MEDIA_URL


class Profile(models.Model):
    user = models.OneToOneField(User,
                                unique=True)
    picture = models.URLField()
    center = models.CharField(max_length=100)

    def __unicode__(self):
        return self.user.username


class Status(models.Model):

    class Meta:
        verbose_name_plural = 'Statuses'


    denomination = models.CharField(max_length=15)
    requests = models.ManyToManyField('Request',
                                      through='RequestStatus')

    def __unicode__(self):
        return self.denomination


class RequestStatus(models.Model):
    is_current = models.BooleanField(default=False)
    status = models.ForeignKey(Status)
    request = models.ForeignKey('Request', related_name='statuses')
    date = models.DateField(auto_now_add=True)
    user = models.ForeignKey(Profile)

    class Meta:
        permissions = (
            ('can_approve_download', _('Can approve a download request')),
        )

    def __unicode__(self):
        return "%s_%s" % (self.request.name, self.status.denomination)

    def save(self, force_insert=False, force_update=False, using=None):
        if self.is_current:
            prev = RequestStatus.objects.filter(request__id=self.request.id,
                                                is_current=True)
            prev.update(is_current=False)

        return super(RequestStatus, self).save(force_insert, force_update,
                                               using)


class RequestManager(models.Manager):

    def get_requests_by_status(self, status):
        return self.model.objects.filter(
            statuses__status__denomination=status,
            statuses__is_current=True).order_by('-priority')


class Request(models.Model):
    name = models.CharField(max_length=100)
    date = models.DateField(auto_now_add=True)
    priority = models.PositiveSmallIntegerField(default=1)
    description = models.TextField()
    filename = models.FileField(upload_to=UPLOAD_FILES_ROOT,
                                blank=True,
                                default='')
    user = models.ForeignKey('Profile')
    tags = models.ManyToManyField('Tag')
    urls = models.ManyToManyField('Url')
    partial = models.BooleanField(default=False,
                                  verbose_name=_('Partial URLs?'),
                                  help_text=_("""The app must download all links
                                              to complete the task"""))
    objects = RequestManager()

    class Meta:
        permissions = (
            ('can_modify_priority', _('Can change the request priority')),
        )

    def __unicode__(self):
        return self.name

    def get_current_status(self):
        return RequestStatus.objects.get(request=self,
                                         is_current=True)

    current_status = property(get_current_status)

    def add_status(self, status, user):
        request_status = RequestStatus()
        request_status.request = self
        request_status.status = Status.objects.get(denomination=status)
        request_status.is_current = True
        request_status.user = user
        request_status.save()

        return request_status

    def download_file(self, url, path):
        local_filename = url.url.split('/')[-1]

        if url.url.startswith('ftp://'):
            pass
        else:
            try:
                credential = Credential.objects.get(url=url)
                r = requests.get(url.url,
                                 stream=True,
                                 verify=False,
                                 auth=(credential.username,
                                       credential.password))

            except Credential.DoesNotExist:
                r = requests.get(url, stream=True, verify=False)

            except Exception as e:
                raise e

        if r.status_code == 200:
            tmp = "%s/%s" % (path, local_filename)

            with open(tmp, 'wb') as f:
                for chunk in r.iter_content(chunk_size=1024):
                    if chunk: # filter out keep-alive new chunks
                        f.write(chunk)
                        f.flush()

            return tmp
        else:
            r.raise_for_status()

    def process(self, user):
        self.add_status('Downloading', user)

        for url in self.urls.all():
            path = '%s/%s' % (DOWNLOAD_PATH, self.name)
            counter = 0

            while os.path.exists(path):
                counter += 1
                path = '%s/%s%s' % (DOWNLOAD_PATH, self.name, str(counter))

            mkdir(path)

            try:
                result = self.download_file(url, path)
                self.filename = result
                self.save()

                self.add_status('Done', user)

                if not self.partial:
                    break

            except Exception as e:
                request_status = self.add_status('Error', user)

                error = RequestError()
                error.url = url
                error.message = e.message
                error.request_status = request_status
                error.save()

                if self.partial:
                    break


    def get_file_url(self):
        site = Site.objects.get_current()

        f = str(self.filename)
        t = f.replace(MEDIA_ROOT, '')
        path = "http://%s%s%s" % (site.domain, MEDIA_URL, t[1:])

        return path

    file_url = property(get_file_url)


class Tag(models.Model):
    denomination = models.CharField(max_length=20)

    def __unicode__(self):
        return self.denomination


class Credential(models.Model):
    username = models.CharField(max_length=100)
    password = models.CharField(max_length=100)
    url = models.OneToOneField('Url')


class Url(models.Model):
    url = models.URLField()

    def __unicode__(self):
        return self.url
