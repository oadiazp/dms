# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand
from apps.core.models import Request
from apps.downloader.tasks import download_a_request


class Command(BaseCommand):

    def handle(self, *args, **options):
        requests = Request.objects.get_requests_by_status('Approved')

        for request in requests:
            download_a_request.delay(request=request)
