from django.core.management.base import LabelCommand
from django.db.models.loading import get_models, get_app

class Command(LabelCommand):
    def handle_label(self, label, **options):
        app = get_app(label)
        models = get_models(app)

        self.stdout.write('from django.contrib import admin\n')
        self.stdout.write('from %s.models import *\n' % label)
        for model in models:
            self.stdout.write('\nadmin.site.register(%s)' % model.__name__)


    