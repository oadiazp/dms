from django.contrib.auth.decorators import permission_required
from django.core.urlresolvers import reverse_lazy
from django.utils.translation import ugettext as _
from django.http import HttpResponse
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView, CreateView, View
from django.views.generic.base import RedirectView
from apps.core.forms import RequestForm, UrlForm
from apps.core.models import Request, Url
from apps.generic.genericviews import JSONListView, GenericCreateView


class RequestsView(TemplateView):
    template_name = 'core/requests.html'

    def dispatch(self, request, *args, **kwargs):
        request.breadcrumbs((
            (_('Home'), reverse_lazy('core_requests'),),
        ))

        return super(RequestsView, self).dispatch(request, *args, **kwargs)


class Home(RedirectView):

    def get_redirect_url(self, **kwargs):
        if self.request.user.is_authenticated():
            return reverse_lazy('core_requests')
        else:
            return reverse_lazy('login')


class ListRequestsView(JSONListView):
    template_name = 'json/request.json'

    def get_context_data(self, **kwargs):
        context = super(ListRequestsView, self).get_context_data(**kwargs)

        context['cant'] = context['object_list'].count()
        context['total'] = Request.objects.filter(
            user__user=self.request.user).count()

        return context

    def get_queryset(self):
        start = self.request.GET.get('iDisplayStart', 0)
        limit = self.request.GET.get('iDisplayLength', 0)

        return Request.objects.filter(user__user=self.request.user)[start:limit]


class AddRequestView(CreateView):
    form_class = RequestForm
    template_name = 'core/add-request.html'

    def dispatch(self, request, *args, **kwargs):
        request.breadcrumbs((
            (_('Home'), reverse_lazy('core_requests'),),
            (_('Add request'), reverse_lazy('core_add_request'),),
        ))

        return super(AddRequestView, self).dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return reverse_lazy('core_request_urls',
                            kwargs={
                                    'id': self.object.id
                                    })

    def get_form_kwargs(self):
        result = super(AddRequestView, self).get_form_kwargs()

        result['profile'] = self.request.user.get_profile()

        return result


class RequestUrlsView(TemplateView):
    template_name = 'core/urls.html'

    def dispatch(self, request, *args, **kwargs):
        r = Request.objects.get(id=kwargs['id'])

        request.breadcrumbs((
            (_('Home'), reverse_lazy('core_requests'),),
            (_('Urls'), reverse_lazy('core_list_request_urls',
                                     kwargs={'id': r.id}),),
        ))

        return super(RequestUrlsView, self).dispatch(request, *args, **kwargs)


    def get_context_data(self, **kwargs):
        context = super(RequestUrlsView, self).get_context_data(**kwargs)

        context['id'] = self.kwargs['id']
        request = Request.objects.get(id=self.kwargs['id'])
        context['form'] = UrlForm(request=request)

        return context


class AddUrlView(GenericCreateView):
    form_class = UrlForm
    response_type = 'json'
    template_name = 'core/add-url.html'

    def get_form_kwargs(self):
        result = super(AddUrlView, self).get_form_kwargs()

        result['request']= Request.objects.get(id=self.kwargs['id'])

        return result


class ListUrlsView(JSONListView):
    template_name = 'json/url.json'

    def get_context_data(self, **kwargs):
        context = super(ListUrlsView, self).get_context_data(**kwargs)

        context['cant'] = context['object_list'].count()
        context['total'] = Url.objects.filter(
            request__id=self.kwargs['id']).count()

        return context

    def get_queryset(self):
        start = self.request.GET.get('iDisplayStart', 0)
        limit = self.request.GET.get('iDisplayLength', 0)

        return Url.objects.filter(request__id=self.kwargs['id'])[start:limit]


class RemoveUrlView(View):

    def post(self, request, *args, **kwargs):
        Url.objects.get(id=self.kwargs['idurl']).delete()

        return HttpResponse(content='')


class RemoveRequestView(View):

    def post(self, request, *args, **kwargs):
        Request.objects.get(id=self.kwargs['id']).delete()

        return HttpResponse(content='')


class ApproveRequestsView(TemplateView):
    template_name = 'core/approve-requests.html'

    def dispatch(self, request, *args, **kwargs):
        request.breadcrumbs((
            (_('Home'), reverse_lazy('core_requests'),),
            (_('Approve requests'), reverse_lazy('core_approve_requests'),),
        ))

        return super(ApproveRequestsView, self).dispatch(request, *args,
                                                         **kwargs)


class UnapprovedRequestsListView(JSONListView):
    template_name = 'json/unapproved-request.json'

    def get_context_data(self, **kwargs):
        context = super(UnapprovedRequestsListView,
                        self).get_context_data(**kwargs)

        context['cant'] = context['object_list'].count()
        context['total'] = Request.objects.get_requests_by_status('New').filter(
            user__user=self.request.user
        ).count()

        return context

    def get_queryset(self):
        start = self.request.GET.get('iDisplayStart', 0)
        limit = self.request.GET.get('iDisplayLength', 0)

        return Request.objects.get_requests_by_status('New').filter(
            user__user=self.request.user)[start:limit]


class ApproveRequest(View):

    @method_decorator(permission_required('can_approve_requests'))
    def post(self, request, *args, **kwargs):
        request = Request.objects.get(id=kwargs['id'])

        request.add_status('Approved', self.request.user.get_profile())

        return HttpResponse()


class DownloadQueueView(TemplateView):
    template_name = 'core/downloads_queue.html'

    @method_decorator(permission_required('can_modify_priority'))
    def dispatch(self, request, *args, **kwargs):
        request.breadcrumbs((
            (_('Home'), reverse_lazy('core_requests'),),
            (_('Download queue'), reverse_lazy('core_queue'),),
        ))

        return super(DownloadQueueView, self).dispatch(request, *args, **kwargs)


class RequestsQueueView(JSONListView):
    template_name = 'json/request-on-queue.json'

    def get_context_data(self, **kwargs):
        context = super(RequestsQueueView, self).get_context_data(**kwargs)

        context['cant'] = context['object_list'].count()
        context['total'] = Request.objects.get_requests_by_status('Approved').filter(
            user__user=self.request.user).count()

        return context

    def get_queryset(self):
        start = self.request.GET.get('iDisplayStart', 0)
        limit = self.request.GET.get('iDisplayLength', 0)

        return Request.objects.get_requests_by_status('Approved').filter(
            user__user=self.request.user)[start:limit]


class PriorityView(View):

    @method_decorator(permission_required('can_modify_priority'))
    def post(self, request, *args, **kwargs):
        download_request = Request.objects.get(id=kwargs['id'])

        if self.request.POST['operation'] == '+':
            download_request.priority += 1
        else:
            download_request.priority -= 1

        download_request.save()

        return HttpResponse()
