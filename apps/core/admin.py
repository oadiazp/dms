from django.contrib import admin
from apps.core.models import *


class RequestStatusInline(admin.TabularInline):
    model = RequestStatus


class RequestErrorsInline(admin.TabularInline):
    model = RequestError


class RequestAdmin(admin.ModelAdmin):
    inlines = [RequestStatusInline]


class RequestStatusAdmin(admin.ModelAdmin):
    inlines = [RequestErrorsInline]

admin.site.register(Profile)
admin.site.register(Request, RequestAdmin)
admin.site.register(Tag)
admin.site.register(Credential)
admin.site.register(Url)
admin.site.register(Status)