from django.contrib.sites.models import Site
from django.core.mail import send_mail
from django.db.models.signals import post_save
from django.dispatch.dispatcher import receiver
from django.template.loader import render_to_string
from apps.core.models import RequestStatus
from apps.downloader.models import RequestError
from dms.settings import EMAIL_HOST_USER, MEDIA_ROOT, MEDIA_URL


@receiver(signal=post_save, sender=RequestStatus)
def send_mail_delegate(sender, instance, created, raw, **kwargs):

    if instance.status.denomination in ['Done']:
        data = {
            'from_email': EMAIL_HOST_USER,
            'fail_silently': True,
            'recipient_list': (
                '%s@uci.cu' % instance.request.user.user.username,)
        }

        if instance.status.denomination == 'Done':
            data['subject'] = render_to_string(
                'downloader/emails/done_subject.txt'
            )

            data['message'] = render_to_string(
                'downloader/emails/done_body.txt',
                {
                    'url': instance.request.file_url
                }
            )

        send_mail(**data)


@receiver(signal=post_save, sender=RequestError)
def send_error_mail(sender, instance, created, raw, **kwargs):
    data = {
        'from_email': EMAIL_HOST_USER,
        'fail_silently': True,
        'recipient_list': (
            '%s@uci.cu' % instance.request_status.request.user.user,)
    }

    data['subject'] = render_to_string(
        'downloader/emails/error_subject.txt'
    )

    data['message'] = render_to_string(
        'downloader/emails/error_body.txt',
        {
            'error': instance.message
        }
    )

    send_mail(**data)
