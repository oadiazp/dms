from django.conf.urls import patterns, url
from django.contrib.auth.views import logout_then_login
from apps.accounts.views import LoginView
from apps.ucistore_finder.views import FindView


urlpatterns = patterns('',
    url(r'find/$', FindView.as_view(), name='ucistore_finder_find'),
)
