from BeautifulSoup import BeautifulSoup
import requests
from dms.settings import FINDER_SERVICE_URL


def parse_response(query):
    params = {'nombre': query,
              'sistema': 'All',
              'view_name': '_ucistore_data',
              'view_display_id': 'page_3'}
    result = requests.post(FINDER_SERVICE_URL, params=params, verify=False)
    result = result.json()
    parsed = BeautifulSoup(result[1]['data'])

    rows = parsed.findAll('tr')

    resp = []

    for row in rows:
        data = row.findAll('td')

        if data:
            resp.append(' '.join(data[0].string.split()))

    return resp