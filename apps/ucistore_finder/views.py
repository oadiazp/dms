
from django.http import HttpResponse
from django.views.generic.base import View
from simplejson import dumps
from apps.ucistore_finder.business import parse_response

class FindView(View):

    def get(self, request, *args, **kwargs):
        query = self.request.GET.get('query', '')
        resp = parse_response(query)
        return HttpResponse(dumps(resp))


