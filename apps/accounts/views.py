from django.contrib.auth import login
from django.core.exceptions import ObjectDoesNotExist
from django.core.urlresolvers import reverse_lazy
from django.views.generic.edit import FormView
from apps.accounts.forms import AuthenticationForm
from apps.core.models import Profile
from dms.settings import IDENTIFICACION_UCI_WSDL


class LoginView(FormView):
    form_class = AuthenticationForm
    template_name = 'accounts/signin_form.html'
    success_url = reverse_lazy('core_requests')


    def form_valid(self, form):
        login(self.request, form.user_cache)

        try:
            profile = self.request.user.get_profile()

        except ObjectDoesNotExist:
            profile = Profile()
            profile.user = self.request.user
            ldap_data = form.user_cache.ldap_user.attrs.data

            profile.center = ldap_data.get('department', '')[0]
            expedient = ldap_data.get('description', '')[0]
            profile.picture = self.get_picture(expedient)
            profile.save()

        return super(LoginView, self).form_valid(form)

    def get_picture(self, ldap_desc):
        from ZSI.client import NamedParamBinding as NPBinding
        from ZSI import EvaluateException
        from BeautifulSoup import BeautifulSoup

        binding = NPBinding(url=IDENTIFICACION_UCI_WSDL)

        try:
            binding.ObtenerFotoDadoIdExpediente(IdExpediente=ldap_desc)

        except EvaluateException as e:
            parsed = BeautifulSoup(binding.data)
            return parsed.find('valorfoto').string

        return '<None>'



