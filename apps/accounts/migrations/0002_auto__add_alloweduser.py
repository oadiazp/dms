# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'AllowedUser'
        db.create_table('accounts_alloweduser', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('username', self.gf('django.db.models.fields.CharField')(max_length=200)),
        ))
        db.send_create_signal('accounts', ['AllowedUser'])


    def backwards(self, orm):
        # Deleting model 'AllowedUser'
        db.delete_table('accounts_alloweduser')


    models = {
        'accounts.alloweduser': {
            'Meta': {'object_name': 'AllowedUser'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        }
    }

    complete_apps = ['accounts']