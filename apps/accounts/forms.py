from django import forms

from django.contrib.auth.forms import AuthenticationForm as DjangoAuthForm
from django.core.exceptions import ObjectDoesNotExist
from apps.accounts.models import AllowedUser
from django.utils.translation import ugettext as _


class AuthenticationForm(DjangoAuthForm):

    def clean(self):
        username = self.cleaned_data.get('username')

        try:
            au = AllowedUser.objects.get(username=username)
            return super(AuthenticationForm, self).clean()

        except ObjectDoesNotExist:
            raise forms.ValidationError(_("This user has no access to the app."))