from django.conf.urls import patterns, url
from django.contrib.auth.views import logout_then_login
from apps.accounts.views import LoginView


urlpatterns = patterns('',
    url(r'login/$', LoginView.as_view(), name='login'),
    url(r'logout/$', logout_then_login, name='logout'),
)
